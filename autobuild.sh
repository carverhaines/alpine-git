#!/bin/bash

echo
echo "Starting new build at: $(date)"
docker pull alpine:latest

currentalpinegitver=$(docker run --rm alpine:latest sh -c "apk --no-cache info git" | grep description | sed 's/^git-\(.*\) description:/\1/g' | cut -f1 -d"-")

currentalpinegitbuild=$(cat README.md | grep "*" | cut -f2 -d"*")

echo "Current alpine git version is $currentalpinegitver "
echo "Current build version is $currentalpinegitbuild "

function version_gt() { test "$(printf '%s\n' "$@" | sort -V | head -n 1)" != "$1"; }

if version_gt $currentalpinegitver $currentalpinegitbuild;
then
    echo "New version of GIT available";
    newgitver=true;
else
    echo "Already up to date at version: $currentalpinegitver";
    newgitver=false;
fi;

if $newgitver;
then
  echo "Building new container on $(date), replacing version number in readme...";
  sleep 2
  #Build and push new container
  docker build -t registry.gitlab.com/carverhaines/alpine-git:$currentalpinegitver .
  docker tag registry.gitlab.com/carverhaines/alpine-git:$currentalpinegitver registry.gitlab.com/carverhaines/alpine-git:latest
  newbuildver=$(docker run -it --rm registry.gitlab.com/carverhaines/alpine-git:latest --version | cut -f3 -d" ")
  docker push registry.gitlab.com/carverhaines/alpine-git:$currentalpinegitver
  docker push registry.gitlab.com/carverhaines/alpine-git:latest
  #Update and push readme
  sed -i "s/$currentalpinegitbuild/$newbuildver/g" README.md
  git add README.md
  git commit -m "Updated to GIT version $newbuildver"
  git push
  echo Completed!
  echo
else
  echo "No need to build container at $(date)";
  echo
  exit;
fi;
