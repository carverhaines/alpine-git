FROM alpine:latest

MAINTAINER Carver <https://gitlab.com/carverhaines>

RUN apk add --no-cache git openssh

VOLUME /git
WORKDIR /git

ENTRYPOINT ["git"]
CMD ["--help"]
