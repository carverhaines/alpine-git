# About
Allows you to use GIT on any computer or server with docker installed, even if git is not installed. Your GIT command runs in the container, then container is destroyed.

Container tags reference version of git used, current version: *2.22.0*

## Usage
First time usage, create an /etc/passwd file that only contains root and your username (to prevent having to load the whole /etc/passwd file to the container):
```
cat /etc/passwd | egrep "root|$USER" | grep -v chroot > /home/$USER/.alpinegitpasswdfile
```
Alternatively you can mount the /etc/passwd in the container alias command: `-v /etc/passwd:/etc/passwd:ro`, though this may have security implications?


The below creates an alias that runs a one-time docker container, sets the UID and GID to your current user, mounts your ssh keys to your home directory (change if your key directory is not home/user/.ssh/), and mounts /etc/passwd so that container can coorelate your UID and username, so it can find the key to use for git, and clone/edit as your local user rather than root.

Add to your .bashrc
```
alias git='docker run -ti --rm -u $(id -u):$(id -g) -v $(pwd):/git -v /home/$USER/.alpinegitpasswdfile:/etc/passwd:ro -v $HOME/.ssh:/home/$USER/.ssh:ro  registry.gitlab.com/carverhaines/alpine-git:latest'
```
Example custom key location: `-v /path/to/keys:/home/$USER/.ssh:ro`

Then use git as you normally would
`git --version`

## Updating
This container repository will autoupdate with the newest version of GIT when available, and version number above will be updated, so recommended adding a cron job or another alias to keep git updated, unless you want a specific version.
```
# Alias:
alias gitupdate='docker pull registry.gitlab.com/carverhaines/alpine-git:latest'
```
```
# Cron:
0 * * * *   docker pull registry.gitlab.com/carverhaines/alpine-git:latest
```
